import asyncio

from io_expander import test_ioexp
from digit import draw_full_number, configure_ioexp_for_digits, test_all_digits, test_all_numbers, update_display_task
from sound import test_sound
from highscores import test_eeprom, dangerous_highscores_test

async def test():
    print("Starting tests...")
    await test_ioexp()
    await test_eeprom()
    # await dangerous_highscores_test()
    # await test_all_digits()
    await test_all_numbers()
    print("Tests complete")
    await test_sound()



async def init():
    print("Initializing...")
    started_tasks = []
    await configure_ioexp_for_digits()
    started_tasks.append(asyncio.create_task(update_display_task()))
    return started_tasks


async def main():
    tasks = await init()
    await test()
    for task in tasks:
        await task

asyncio.run(main())