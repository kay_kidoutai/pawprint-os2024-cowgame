import asyncio

import board
import audiomp3
import audiopwmio

_AUDIO = audiopwmio.PWMAudioOut(board.GP0)


async def test_sound():
    decoder = audiomp3.MP3Decoder(open("sounds/uwu.mp3", "rb"))
    _AUDIO.play(decoder)
    while _AUDIO.playing:
        await asyncio.sleep(0)  # Yield

print("Done playing!")