from io_expander import CMDB_OUT_PORT0, CMDB_OUT_PORT1, CMDB_CFG_PORT0, CMDB_CFG_PORT1, ioexp_write_reg
import asyncio


# This is a global. It kills me to do this, but this is circuitpython and I think it makes sense here
DISPLAY_VALUE = 0

# Port 0
IOEXP_A_PIN = 0
IOEXP_B_PIN = 1
IOEXP_C_PIN = 2
IOEXP_D_PIN = 3
IOEXP_DIG0_SEL_PIN = 4
IOEXP_DIG1_SEL_PIN = 5
IOEXP_DIG2_SEL_PIN = 6
IOEXP_DIG3_SEL_PIN = 7

# Port 1
IOEXP_DIG4_SEL_PIN = 0
IOEXP_DIG5_SEL_PIN = 1
IOEXP_DIG6_SEL_PIN = 2
IOEXP_DIG7_SEL_PIN = 3
IOEXP_DIG8_SEL_PIN = 4
IOEXP_DIG9_SEL_PIN = 5

DIGIT_SHIFT_VALUES = [
        IOEXP_DIG0_SEL_PIN,
        IOEXP_DIG1_SEL_PIN,
        IOEXP_DIG2_SEL_PIN,
        IOEXP_DIG3_SEL_PIN,
        IOEXP_DIG4_SEL_PIN,
        IOEXP_DIG5_SEL_PIN,
        IOEXP_DIG6_SEL_PIN,
        IOEXP_DIG7_SEL_PIN,
        IOEXP_DIG8_SEL_PIN,
        IOEXP_DIG9_SEL_PIN
    ]

MAX_INSTALLED_DIGITS = 10


async def configure_ioexp_for_digits():
    await ioexp_write_reg(CMDB_CFG_PORT0, 0x0)
    await ioexp_write_reg(CMDB_CFG_PORT1, 0x0)


async def build_port_values(number: int, digit: int) -> tuple:
    if 0 > number > 9:
        raise ValueError(f"Cannot draw the number {number} on a single digit")
    if 0 > digit >= MAX_INSTALLED_DIGITS:
        raise ValueError(f"Digit number {digit} is invalid. Must be > 0 and < {MAX_INSTALLED_DIGITS}")
    port_0 = number & 0xF  # bottom 4 bits are the number
    port_1 = 0x0
    if digit > 3:
        port_1 = (1 << DIGIT_SHIFT_VALUES[digit]) & 0xFF
    else:
        port_0 = port_0 | (1 << DIGIT_SHIFT_VALUES[digit]) & 0xF0
    return port_0, port_1


async def test_single_digit(digit: int):
    print(f"Testing digit {digit}")
    for number in range (0, 10):
        print(f"Testing number {number}")
        port_0, port_1 = await build_port_values(number=number, digit=digit)
        print(f"port_0: {bin(port_0)}, port_1: {bin(port_1)}")
        await ioexp_write_reg(CMDB_OUT_PORT0, port_0)
        await ioexp_write_reg(CMDB_OUT_PORT1, port_1)
        await asyncio.sleep(.05)


async def test_all_digits():
    print("Starting all digits test...")
    for digit in range(0, MAX_INSTALLED_DIGITS):
        await test_single_digit(digit=digit)


async def test_all_numbers():
    print("Starting all numbers test...")
    for number in range(0, 10):
        global DISPLAY_VALUE
        DISPLAY_VALUE = number * 1111111111
        print(f"Testing value {DISPLAY_VALUE}...")
        await asyncio.sleep(.5)
        print("slept")


async def draw_full_number(number: int):
    if number > ((10 ** MAX_INSTALLED_DIGITS) - 1):
        raise ValueError(f"Largest digit we can display on a {MAX_INSTALLED_DIGITS} display is {(10 ** MAX_INSTALLED_DIGITS) - 1}")
    # Not proud of this hack, but it works well in python
    numbers = [digit for digit in f"{number}"]
    while len(numbers) < MAX_INSTALLED_DIGITS:
        numbers = ['0'] + numbers
    
    for digit in range(0, MAX_INSTALLED_DIGITS):
        port_0, port_1 = await build_port_values(number=int(numbers[digit]), digit=digit)
        await ioexp_write_reg(CMDB_OUT_PORT0, port_0)
        await ioexp_write_reg(CMDB_OUT_PORT1, port_1)
        await asyncio.sleep(.001)  # Reduces flicker


async def update_display_task():
    while True:
        await draw_full_number(DISPLAY_VALUE)