import microcontroller
import busio
import digitalio
from adafruit_bus_device.i2c_device import I2CDevice
from adafruit_24lc32 import EEPROM_I2C
import random
import time

HIGHSCORES = [0, 0, 0, 0, 0]  # Another global. Hate it, but this is circuitpython

I2C_BUSADDR = 0b1010000
TEST_ADDRESS = 0x0
HIGHSCORE_START_ADDRESS = 0x10
HIGHSCORE_WIDTH_BYTES = 0x2
NUM_HIGHSCORES_STORED = 5

SCL_PIN = microcontroller.pin.GPIO27
SDA_PIN = microcontroller.pin.GPIO26
WP_PIN = microcontroller.pin.GPIO28
WP_SIG = digitalio.DigitalInOut(WP_PIN)
WP_SIG.direction = digitalio.Direction.OUTPUT

_EEPROM = EEPROM_I2C(
    i2c_bus=busio.I2C(SCL_PIN, SDA_PIN, frequency=400_000),
    address=I2C_BUSADDR,
    write_protect=True,
    wp_pin=WP_SIG,
    max_size=0x4000
)


async def eeprom_write_16bit(address: int, data: int) -> None:
    _EEPROM.write_protected = False
    print(f"Writing 0x{data:.2x} to 0x{address:.2x}")
    _EEPROM[address] = data & 0xFF
    _EEPROM[address + 1] = (data & 0xFF00) >> 8
    print(f"W LOW: {_EEPROM[address]} : {data & 0xFF:.2x}")
    print(f"W HIGH: {_EEPROM[address + 1]} : {(data & 0xFF00) >> 8:.2x}")
    _EEPROM.write_protected = True


async def eeprom_read_16bit(address: int) -> int:
    low_byte = int.from_bytes(_EEPROM[address], "little")
    high_byte = int.from_bytes(_EEPROM[address + 1], "little")
    print(f"R LOW: {low_byte:.2x}")
    print(f"R HIGH: {high_byte:.2x}")
    data = (high_byte << 8) | low_byte
    print(f"read 0x{data:.2x} from address 0x{address:.2x}")
    return data


async def test_eeprom():
    print("Testing EEPROM...")
    for address in range(TEST_ADDRESS, HIGHSCORE_START_ADDRESS, 0x2):
        for test_data in [0xdead, 0xbeef]:
            print(f"Testing address 0x{address:.2x}...")
            await eeprom_write_16bit(address=address, data=test_data)
            written_data = await eeprom_read_16bit(address=address)
            if written_data != test_data:
                raise RuntimeError(f"ERROR: Expected 0x{test_data:.2x} at 0x{address:.2x}, got 0x{written_data:.2x}")
                return False


async def read_highscores():
    highscores = []
    for address in range(HIGHSCORE_START_ADDRESS, HIGHSCORE_START_ADDRESS + (NUM_HIGHSCORES_STORED * 0x2), 0x2):
        highscores.append(await eeprom_read_16bit(address))
    print(highscores)
    highscores.sort()
    highscores.reverse()
    return highscores


async def init_highscores():
    global HIGHSCORES
    HIGHSCORES = await read_highscores()
    print(f"Current Highscores: {', '.join(str(score) for score in HIGHSCORES)}")


async def update_highscores(new_highscore: int):
    global HIGHSCORES
    do_update = False
    for highscore in HIGHSCORES:
        if new_highscore > highscore:
            do_update = True
            break
    if do_update:
        print(f"New highscore: {new_highscore}")
        HIGHSCORES.append(new_highscore)
        HIGHSCORES.sort()
        HIGHSCORES.reverse()
        HIGHSCORES = HIGHSCORES[:-1]
        highscore_index = 0
        for address in range(HIGHSCORE_START_ADDRESS, HIGHSCORE_START_ADDRESS + (NUM_HIGHSCORES_STORED * 0x2), 0x2):
            await eeprom_write_16bit(address=address, data=0xdead)
            new_data = await eeprom_read_16bit(address=address)
            if new_data != HIGHSCORES[highscore_index]:
                raise RuntimeError(f"0x{new_data:.2x} != {HIGHSCORES[highscore_index]:.2x}")
            highscore_index += 1


async def dangerous_highscores_test():
    global HIGHSCORES
    restore_highscores = HIGHSCORES
    print("Testing high scores...")
    print(f"Current Highscores: {', '.join(str(score) for score in restore_highscores)}")
    # Reset all high scores to zero in the EEPROM
    for address in range(HIGHSCORE_START_ADDRESS, HIGHSCORE_START_ADDRESS + (NUM_HIGHSCORES_STORED * 0x2), 0x2):
        await eeprom_write_16bit(address=address, data=0)
    random.seed(time.time())
    for i in range(0, 10):
        new_score = random.randrange(0, 300)
        print(f"Inserting new score of {new_score}")
        await update_highscores(new_highscore=new_score)
        await init_highscores()
    # Restore old high scores
    highscore_index = 0
    for address in range(HIGHSCORE_START_ADDRESS, HIGHSCORE_START_ADDRESS + (NUM_HIGHSCORES_STORED * 0x2), 0x2):
        await eeprom_write_16bit(address=address, data=restore_highscores[highscore_index])
        highscore_index += 1
    await init_highscores()