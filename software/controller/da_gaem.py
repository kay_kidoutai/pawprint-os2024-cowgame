import board
import digitalio
from switch_matrix import SwitchMatrix
from uart_score_display import send_scores


class DaGaem:
    def __init__(self, existing_highscore: int = 0):
        self._highscore = existing_highscore
        self._switch_matrix = SwitchMatrix()
        self._newgame_switch = digitalio.DigitalInOut(board.GP18)
        self._newgame_switch.pull = digitalio.Pull.DOWN
        self._newgame_switch.direction = digitalio.Direction.INPUT
        self._running = True

    async def game_loop(self):
        while self._running:
            # Scan the switch matrix looking for new bottles that have been hit
            await self._switch_matrix.scan_all_switches()
            new_score = self._switch_matrix.get_score()
            if new_score is not None:
                # We hit a bottle, update the score
                await send_scores(score=new_score, highscore=self._highscore)
            # If the new game switch is pressed, reset the game
            if self._newgame_switch.value:
                final_score = await self._switch_matrix.reset_game()
                if final_score > self._highscore:
                    self._highscore = final_score

    async def stop(self):
        self._running = False
