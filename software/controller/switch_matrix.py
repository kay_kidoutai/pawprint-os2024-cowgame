import time
import board
import digitalio

SCORE_INCREASE_AMOUNT = 350


class SwitchMatrix:
    def __init__(self):
        self._score = 0
        self._has_new_score = True
        self._ko = []
        self._init_all_pins()

    def get_score(self):
        if self._has_new_score:
            return self._score
        return None

    def _init_all_pins(self):
        # denotes the horizontal inputs
        H0 = digitalio.DigitalInOut(board.GP1)
        H0.direction = digitalio.Direction.INPUT
        H0.pull = digitalio.Pull.DOWN

        H1 = digitalio.DigitalInOut(board.GP2)
        H1.direction = digitalio.Direction.INPUT
        H1.pull = digitalio.Pull.DOWN

        H2 = digitalio.DigitalInOut(board.GP3)
        H2.direction = digitalio.Direction.INPUT
        H2.pull = digitalio.Pull.DOWN

        # Need these for UART
        # H3 = digitalio.DigitalInOut(board.GP4)
        # H3.direction = digitalio.Direction.INPUT
        # H3.pull = digitalio.Pull.DOWN
        #
        # H4 = digitalio.DigitalInOut(board.GP5)
        # H4.direction = digitalio.Direction.INPUT
        # H4.pull = digitalio.Pull.DOWN

        # Need this for display ready signal
        # H5 = digitalio.DigitalInOut(board.GP6)
        # H5.direction = digitalio.Direction.INPUT
        # H5.pull = digitalio.Pull.DOWN

        H6 = digitalio.DigitalInOut(board.GP7)
        H6.direction = digitalio.Direction.INPUT
        H6.pull = digitalio.Pull.DOWN

        H7 = digitalio.DigitalInOut(board.GP8)
        H7.direction = digitalio.Direction.INPUT
        H7.pull = digitalio.Pull.DOWN
        # denotes the vertical outputs
        V0 = digitalio.DigitalInOut(board.GP9)
        V0.direction = digitalio.Direction.OUTPUT
        V0.value = False

        V1 = digitalio.DigitalInOut(board.GP10)
        V1.direction = digitalio.Direction.OUTPUT
        V1.value = False

        V2 = digitalio.DigitalInOut(board.GP12)
        V2.direction = digitalio.Direction.OUTPUT
        V2.value = False

        V3 = digitalio.DigitalInOut(board.GP13)
        V3.direction = digitalio.Direction.OUTPUT
        V3.value = False

        V4 = digitalio.DigitalInOut(board.GP14)
        V4.direction = digitalio.Direction.OUTPUT
        V4.value = False

        V5 = digitalio.DigitalInOut(board.GP15)
        V5.direction = digitalio.Direction.OUTPUT
        V5.value = False

        V6 = digitalio.DigitalInOut(board.GP16)
        V6.direction = digitalio.Direction.OUTPUT
        V6.value = False

        V7 = digitalio.DigitalInOut(board.GP17)
        V7.direction = digitalio.Direction.OUTPUT
        V7.value = False

        # creates two lists of our pins
        self._hozi = [(0, H0), (1, H1), (2, H2), (6, H6), (7, H7)]
        self._vert = [(0, V0), (1, V1), (2, V2), (3, V3), (4, V4), (5, V5), (6, V6), (7, V7)]

    async def _increase_score(self, amount: int) -> None:
        self._score += amount
        self._has_new_score = True

    async def reset_game(self) -> int:
        score = self._score
        self._ko = []
        self._score = 0
        self._has_new_score = True
        return score

    async def scan_all_switches(self):
        # make empty list or a way to keep track of what has been knocked down.
        # score should not increase unless a new pin is knocked
        # time.sleep(.1)
        for index_x, x in self._vert:
            # print("Checking Vertical", index_x, "...")
            x.value = True
            for index_y, y in self._hozi:
                if y.value:
                    # print("Row ", index_x, " & Column ", index_y, " is hit")
                    await self.check_bottle_hit(index_x, index_y)
            x.value = False
            # print("Total score: ", self._score)

    async def check_bottle_hit(self, x: int, y: int) -> None:
        if (x, y) not in self._ko:
            await self._increase_score(amount=SCORE_INCREASE_AMOUNT)
            self._ko.append((x, y))
