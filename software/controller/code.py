import asyncio
from sound import test_sound
from uart_score_display import test_display
from da_gaem import DaGaem


async def test():
    await asyncio.sleep(2)
    print("Starting tests...")
    await asyncio.sleep(2)
    await test_display()
    await asyncio.sleep(2)
    # await test_sound()
    print("Tests complete")


async def main():
    await test()
    gaem = DaGaem()
    await gaem.game_loop()

asyncio.run(main())