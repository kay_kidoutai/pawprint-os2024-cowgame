import board
import busio
import digitalio

DISPLAY_READY = digitalio.DigitalInOut(board.GP6)
DISPLAY_READY.direction = digitalio.Direction.INPUT
DISPLAY_READY.pull = digitalio.Pull.DOWN
# We use H3 for TX and H4 for RX. That means connect the RX pin from the score display to the H3 pin on the controller
# You do not need to connect H4. I had to specify it here for the code to work
SCORE_UART = busio.UART(board.GP4, board.GP5, baudrate=115200)
NUM_DIGITS = 8
MAX_SCORE = (10 ** (NUM_DIGITS / 2)) - 1


async def send_scores(score: int, highscore: int):
    # print(f"score: {score}, highscore {highscore}")
    send_data = ""
    for a_score in (score, highscore):
        a_score = "".join(reversed(str(a_score)))
        while len(a_score) < NUM_DIGITS/2:
            a_score += "0"
        send_data += a_score
    SCORE_UART.write(send_data + "\n")


def await_display_ready():
    while not DISPLAY_READY.value:
        pass


async def test_display():
    for test_score in range(0, int(MAX_SCORE)):
        await send_scores(test_score, test_score)
