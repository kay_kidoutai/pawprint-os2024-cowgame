import microcontroller
import busio
from adafruit_bus_device.i2c_device import I2CDevice
I2C_BUSADDR = 0b0100000
SCL_PIN = microcontroller.pin.GPIO21
SDA_PIN = microcontroller.pin.GPIO20
CMDB_IN_PORT0 = b'\x00'
CMDB_IN_PORT1 = b'\x01'
CMDB_OUT_PORT0 = b'\x02'
CMDB_OUT_PORT1 = b'\x03'
CMDB_POLINV_PORT0 = b'\x04'
CMDB_POLINV_PORT1 = b'\x05'
CMDB_CFG_PORT0 = b'\x06'
CMDB_CFG_PORT1 = b'\x07'
_IOEXP_I2C_DEVICE = I2CDevice(busio.I2C(SCL_PIN, SDA_PIN, frequency=400_000), I2C_BUSADDR)


async def ioexp_write_reg(register: bytes, data: int) -> None:
    with _IOEXP_I2C_DEVICE:
        data = bytearray(register) + bytearray(data.to_bytes(1, "little"))
        _IOEXP_I2C_DEVICE.write(buffer=data)


async def ioexp_read_reg(register: bytes) -> int:
    bytes_read = bytearray(1)
    bytes_write = bytearray(register)
    with _IOEXP_I2C_DEVICE:
        _IOEXP_I2C_DEVICE.write_then_readinto(out_buffer=bytes_write, in_buffer=bytes_read)
    return int.from_bytes(bytes_read, "little")


async def test_ioexp():
    print("Testing IO Expander...")
    for register in [CMDB_CFG_PORT0, CMDB_CFG_PORT1]:
        print(f"Testing register {register}...")
        register_revert = await ioexp_read_reg(register)
        for bit in range(0, 8):
            await ioexp_write_reg(register=register, data=(1 << bit))
            written_register = await ioexp_read_reg(register=register)
            if written_register != (1 << bit):
                print(f"ERROR: Register {register} expected to be {1 << bit}, instead is {written_register}")
                return False
        await ioexp_write_reg(register=register, data=register_revert)
    return True
