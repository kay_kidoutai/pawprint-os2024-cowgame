/*
Theory of Operation:
This display matrix is programmed to drive up to 10 seven segment digits at 
once. Each seven segment digit is wired to the same A, B, C, and D pin. The
A, B, C, and D pins determine the value of the digit. They represent the 
number to be displayed in binary. A is bit 0, B is bit 1, and so on. If we
wanted to write the value 6 to a digit, we would set A to 0, B to 1, C to 1,
and D to 0. This gives us 0110, or 6 in binary.

Whether or not each digit is on is determined by the D0 - D9 pins. This 
allows us to update the ABCD pins for a given digit, show it briefly, then 
update ABCD for the next digit, show that one briefly, and so on. If we 
complete this loop fast enough, persistence of vision kicks in and it 
appears as if all the digits are on at the same time. 

A matrixed driver like this relies on very fast operation. Many parts of the
code have been optimized for speed. For example, we do not use the Arduino 
pinMode or digitalRead functions. Instead, we configure our pins and check 
their values directly using the ATMega328's PORT and DDR registers. This 
method (while being a bit more dangerous) is up to 50 times faster than the
Arduino libraries. 

Updating the display is done over UART. To update the display, send a 
string of numbers to the display terminated with a newline. If you send
more numbers than there are digits, then it just starts to overwrite the 
numbers you have already sent. If you want a digit to not show anything, 
then send a 255 (0xFF in hexadecimal). The UART speed is 115200 bits 
per second. This is much faster than the default 9600, which helps the 
display refresh rate stay high.
*/


// A:     PB0 (D8)
// B:     PB1 (D9)
// C:     PB2 (D10)
// D:     PB3 (D11)
// D0:    PB4 (D12)
// D1:    PB5 (D13)

// D2:    PC0 (A0)
// D3:    PC1 (A1)
// D4:    PC2 (A2)
// D5:    PC3 (A3)
// D6:    PC4 (A4)
// D7:    PC5 (A5)
// D8:    PC6 (A6)
// D9:    PC7 (A7)

#define DEBUG 0

// READY: PD7 (D7)

// B Port Registers
#define NUM_A (1 << 0)
#define NUM_B (1 << 1)
#define NUM_C (1 << 2)
#define NUM_D (1 << 3)
#define SEL_0 (1 << 4)
#define SEL_1 (1 << 5)

// C Port Registers
#define SEL_2 (1 << 0)
#define SEL_3 (1 << 1)
#define SEL_4 (1 << 2)
#define SEL_5 (1 << 3)
#define SEL_6 (1 << 4)
#define SEL_7 (1 << 5)
#define SEL_8 (1 << 6)
#define SEL_9 (1 << 7)

// D Port Registers
#define READY (1 << 7)
// The number of digits connected to the display
#define NUM_DIGITS  8
#define BLANK_TOKEN 'X'

// All the digit shift values in a convenient array for indexed access
uint8_t digit_shift_values[] = {SEL_0, SEL_1, SEL_2, SEL_3, SEL_4, SEL_5, SEL_6, SEL_7, SEL_8, SEL_9};

// Store the digits we are recieving and showing
uint8_t digit_buffers[2][NUM_DIGITS] = {{0},{0}};
// The index for tracking where in the buffer we want to write to
uint8_t recv_buffer_index = 0;
// Point to the current active buffer for showing the digits
// The buffer for recieving the digits is the XOR of this value
uint8_t active_buffer = 0;

// Function to dump out the values of our active and digit buffers
// This is for debug only
void dump_buffers(){
  Serial.println("Digit buffers:");
  Serial.print("Active buffer: ");
  Serial.println(active_buffer, HEX);
  for(uint8_t i = 0; i < 2; i++){
    for(uint8_t j = 0; j < NUM_DIGITS; j++){
      Serial.print(digit_buffers[i][j], HEX);
      Serial.print(" ");
    }
    Serial.println();
  }

}

void setup() {
  // put your setup code here, to run once:
  // Set up DDRB so that all our pins we're using are outputs
  DDRB = (NUM_A | NUM_B | NUM_C | NUM_D | SEL_0 | SEL_1);
  // Set up DDRC so that all our pins we're using are outputs
  DDRC = (SEL_2 | SEL_3 | SEL_4 | SEL_5 | SEL_6 | SEL_7 | SEL_8 | SEL_9);
  // Set up the DDRD so that all our pins we're using are outputs
  // Be careful not to mess up serial here, do a read modify write
  DDRD = DDRD | READY;

  // Open the serial port
  Serial.begin(115200);

  // Populate the digit buffer with a single zero, rest of the digits are off
  // Initialize our buffers so we display a single 0 on boot
  for(uint8_t i = 0; i < NUM_DIGITS; i++){
    // Setting an index to BLANK_TOKEN means don't show any digits at all
    digit_buffers[active_buffer][i] = BLANK_TOKEN;
    digit_buffers[active_buffer^1][i] = BLANK_TOKEN;
  }
  digit_buffers[active_buffer][0] = 0;

  // Tell the other system we are ready
  PORTD = PORTD | READY;
}

// This function sets our PORTB and PORTC registers so they can be output to the display matrix
void set_port_vals(uint8_t digit, uint8_t number){
  // Zero out the ports
  PORTB = 0x0;
  PORTC = 0x0;
  // BLANK_TOKEN means the digit is off. Just exit
  if(number == BLANK_TOKEN){
    delay(1); // Put the same delay here so the function takes the same amount of time. Otherwise, it changes the brightness because the display is off for a shorter amount of time
    return;
  }
  // The bottom four bits of the port are the number value we want to show on the digit
  uint8_t port_b = (number & 0xF);
  uint8_t port_c = 0x0;
  // Any digit greater than digit 1 is controlled by the PORTC register
  if(digit > 1){
    port_c = digit_shift_values[digit];
  }
  // Digits 0 and 1 are controleld by PORTB
  else{
    port_b = port_b | digit_shift_values[digit];
  }

  // Write the values
  PORTB = port_b;
  PORTC = port_c;
  // Delay for a millisecond so the value shows up on the display
  delay(1);
    // Zero out the ports
  PORTB = 0x0;
  PORTC = 0x0;
}

// Disable all output to the display
void disable_all_digits(){
  PORTB = 0x0;
  PORTC = 0x0;
}

// Handle each digit in the display, one by one
void handle_digits(){
  for(uint8_t i = 0; i < NUM_DIGITS; i++){
    set_port_vals(i, digit_buffers[active_buffer][i]);
  }
}

// Check for serial input. If there is serial data, it means someone is trying to update the display. 
void handle_serial_input(){
  // Blank out all the digits
  disable_all_digits();
  // Read a single character from serial
  int in = Serial.read();
  // If there is a char, do work
  if(in != -1){
    // Change to a byte so it's easier to work with
    uint8_t data = in & 0xFF;
    if(DEBUG){
      Serial.print("Got char ");
      Serial.println(in, HEX);
    }
    // If we get a newline, then we have the full number to display
    if(data == '\n'){
      // Zero out the rest of the recv buffer with BLANK_TOKEN so no garbage gets displayed
      if(DEBUG){Serial.println("Got a newline");};
      for(uint8_t i = recv_buffer_index; i < NUM_DIGITS; i++){
        digit_buffers[1^active_buffer][i] = BLANK_TOKEN;
      }
      if(DEBUG){dump_buffers();};
      // Swap the recv and display buffers
      // Doing and XOR like this to flip the 0 and 1 is way faster than using if statements
      active_buffer = active_buffer ^ 1;
      // Reset the recv index
      recv_buffer_index = 0;
    }
    // If we get a number or a blank, write it to our buffer
    else if(('0' < data < '9') || data == BLANK_TOKEN){
      digit_buffers[1^active_buffer][recv_buffer_index] = data;
      recv_buffer_index++;
      // Prevent overflow
      if(recv_buffer_index > NUM_DIGITS){
        recv_buffer_index = 0;
      }
    }
    // Anything else that's not a char number is not stored to the buffer
  }
}

void loop() {
  handle_digits();
  handle_serial_input();
}
